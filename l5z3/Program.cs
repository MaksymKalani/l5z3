﻿using Microsoft.EntityFrameworkCore;
using System;

namespace l5z3
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var context = new StudentsContext())
            {
                foreach(var s in context.Students.Include(x => x.Group))
                {
                    Console.WriteLine($"{s.Id} {s.Name} {s.Group.Name}");
                }
            }
            
        }
    }
}
